package com.o18gxx.proyecto.modelo_logica.repository_service;

import com.o18gxx.proyecto.controlador_persistencia.Producto;

import java.util.List;

public interface ProductoServicio {

    public Producto save(Producto producto);
    public List<Producto> findAll();
    public Producto findById(Integer idProd);
    public void delete(Integer idProd);
}
