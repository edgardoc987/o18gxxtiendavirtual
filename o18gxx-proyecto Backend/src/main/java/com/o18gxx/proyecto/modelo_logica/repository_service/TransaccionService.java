package com.o18gxx.proyecto.modelo_logica.repository_service;

import com.o18gxx.proyecto.controlador_persistencia.Transaccion;

import java.util.List;

public interface TransaccionService {

    public Transaccion save(Transaccion transaccion);
    public void delete(Integer idTrans);
    public Transaccion findById(Integer idTrans);
    public List<Transaccion> findAll();
}
