package com.o18gxx.proyecto.modelo_logica;

import com.o18gxx.proyecto.controlador_persistencia.Transaccion;
import org.springframework.data.repository.CrudRepository;

public interface TransaccionCrudRepositoryDao extends CrudRepository<Transaccion, Integer> {
}
