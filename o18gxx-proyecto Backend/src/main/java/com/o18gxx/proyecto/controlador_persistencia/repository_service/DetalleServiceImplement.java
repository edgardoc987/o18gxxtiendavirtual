package com.o18gxx.proyecto.controlador_persistencia.repository_service;

import com.o18gxx.proyecto.controlador_persistencia.Detalle;
import com.o18gxx.proyecto.modelo_logica.DetalleCrudRepositoryDao;
import com.o18gxx.proyecto.modelo_logica.repository_service.DetalleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class DetalleServiceImplement implements DetalleService {
    // Atributos
    @Autowired
    private DetalleCrudRepositoryDao detalleCrudRepositoryDao;

    // Métodos - Funciones
    @Override
    @Transactional
    public Detalle save(Detalle detalle){
        return detalleCrudRepositoryDao.save(detalle);
    }

    @Override
    @Transactional
    public void delete(Integer idDet){
        detalleCrudRepositoryDao.deleteById(idDet);
    }

    @Override
    @Transactional
    public Detalle findById(Integer idDet){
        return detalleCrudRepositoryDao.findById(idDet).orElse(null);
    }

    @Override
    @Transactional
    public List<Detalle> findAll(){
        return (List<Detalle>) detalleCrudRepositoryDao.findAll();
    }
}
