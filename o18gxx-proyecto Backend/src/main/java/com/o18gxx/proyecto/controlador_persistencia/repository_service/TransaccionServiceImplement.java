package com.o18gxx.proyecto.controlador_persistencia.repository_service;

import com.o18gxx.proyecto.controlador_persistencia.Transaccion;
import com.o18gxx.proyecto.modelo_logica.TransaccionCrudRepositoryDao;
import com.o18gxx.proyecto.modelo_logica.repository_service.TransaccionService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

public class TransaccionServiceImplement implements TransaccionService {

    // Atributo
    @Autowired
    private TransaccionCrudRepositoryDao transaccionCrudRepositoryDao;

    // Métodos - Funciones
    @Override
    @Transactional
    public Transaccion save(Transaccion transaccion){
        return transaccionCrudRepositoryDao.save(transaccion);
    }

    @Override
    @Transactional
    public void delete(Integer idTrans){
        transaccionCrudRepositoryDao.deleteById(idTrans);
    }

    @Override
    @Transactional
    public Transaccion findById(Integer idTrans){
        return transaccionCrudRepositoryDao.findById(idTrans).orElse(null);
    }

    @Override
    @Transactional
    public List<Transaccion> findAll(){
        return (List<Transaccion>) transaccionCrudRepositoryDao.findAll();
    }

}
